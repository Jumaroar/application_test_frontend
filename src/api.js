import NProgress from 'nprogress';
import axios from 'axios';

const api = axios.create({
  baseURL: process.env.VUE_APP_ROOT_API+process.env.VUE_APP_API_VERSION,
})

// before a request is made start the nprogress
api.interceptors.request.use(config => {
  NProgress.start()
  return config
})

// before a response is returned stop nprogress
api.interceptors.response.use(response => {
  NProgress.done()
  return response
})

export default api