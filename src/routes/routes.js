import Home from '@/components/Home'
import Rsa from '@/components/Rsa'
export default [
  {
    path: '/',
    name: 'Home',
    component: Home,
    title: process.env.VUE_APP_TITLE
  },
  {
      path:'/create',
      name:'create',
      component: () => import('../views/Create.vue'),
      title: "Create Key"
  },
  {
      path:'/rsa-ops',
      name:'rsa_ops',
      component: Rsa,
      title: "Encrypt & Decrypt Text"
  }
]