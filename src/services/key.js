import api from "../api";

class Key {
    async getKeys (query = "") {
        try {
            const result = await api.get('rsa', {params:{query}});
            return {success:true,data:result.data}
        } catch(error) {
            return {success:false,error}
        }
    }

    async create (key) {
        try {
            const result = await api.post('rsa/create', key)
            return {success:true,data:result.data}
        } catch (error) {
            return {success:false,error}
        }
    }

    async cypher (data) {
        try {
            const result = await api.post('rsa/cypher', data)
            return {success:true,data:result.data}
        } catch (error) {
            return {success:false,error}
        }
    }

    async decypher (data) {
        try {
            const result = await api.post('rsa/decypher', data)
            return {success:true,data:result.data}
        } catch (error) {
            return {success:false,error}
        }
    }


}

export default new Key()
