import EventBus from "../buses/event_bus"
const helper = {
    methods:{
        setResponse(request){
            if (request.success) {
                return {success:request.success, response_text:request.data.content, error:""}
            } else {
                return {success:request.success, error:request.error.response.data.message}                
            }
        },
        setAlert(request){
            EventBus.$emit('show_alert', request)
        }
    }
}

export default helper